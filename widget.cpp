﻿#include "widget.hpp"
#include "ui_widget.h"
#include <QFile>
#include <QMessageBox>
#include <QTime>
#include <QtMath>
#include "random.hpp"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    setTableWidgetHeaders();
    fillUserInputTable();
}

Widget::~Widget()
{
    delete ui;
}

void Widget::setTableWidgetHeaders()
{
    ui->tableWidgetAlgo->setHorizontalHeaderItem(0, new QTableWidgetItem("0"));
    ui->tableWidgetAlgo->setHorizontalHeaderItem(1, new QTableWidgetItem("00"));
    ui->tableWidgetAlgo->setHorizontalHeaderItem(2, new QTableWidgetItem("000"));

    ui->tableWidgetTable->setHorizontalHeaderItem(0, new QTableWidgetItem("0"));
    ui->tableWidgetTable->setHorizontalHeaderItem(1, new QTableWidgetItem("00"));
    ui->tableWidgetTable->setHorizontalHeaderItem(2, new QTableWidgetItem("000"));

    ui->tableWidgetUser->setHorizontalHeaderItem(0, new QTableWidgetItem("0"));
    ui->tableWidgetUser->setHorizontalHeaderItem(1, new QTableWidgetItem("00"));
    ui->tableWidgetUser->setHorizontalHeaderItem(2, new QTableWidgetItem("000"));
}

QVector<int> Widget::loadFile(const QString path)
{
    if (!QFile::exists(path)) {
        throw QString("File not exists.");
    }

    QFile file(path);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        throw QString("File open error.");
    }

    QTextStream in(&file);
    QVector<int> result;
    int i;
    while (!in.atEnd()) {
        in >> i;
        result.append(i);
    }
    file.close();

    return result;
}

void Widget::fillUserInputTable()
{
    for (int i = 0; i < ui->tableWidgetUser->rowCount(); i++) {
        ui->tableWidgetUser->setItem(i, 0, new QTableWidgetItem(QString::number(i)));
        ui->tableWidgetUser->setItem(i, 1, new QTableWidgetItem(QString::number(1)));
        ui->tableWidgetUser->setItem(i, 2, new QTableWidgetItem(QString::number(i % 2)));
    }
}

/*!
 * \brief Widget::chiSquared Хи-Квардрат (из Кнута)
 * \param vector
 * \param digitCount количество разрядов
 * \return
 */
double Widget::chiSquared(const QVector<int> vector, int digitCount)
{
    // Ys -- число наблюдений, которые относятся к категории s
    QVector<int> Ys = QVector<int>(qFloor(qPow(10, digitCount)), 0);
    for (auto it = vector.begin(); it != vector.end(); it++) {
        Ys[*it]++;
    }
    // ps -- вероятность того, что каждое наблюдение относится к категории s
    double ps = 1.0 / Ys.size();
    double sum = 0.0;
    for (auto it = Ys.begin(); it != Ys.end(); it++) {
        sum += double((*it) * (*it)) / ps;
    }
    double V = (1.0 / vector.size() * sum - vector.size()) / qFloor(qPow(10, digitCount));
    return V;
}

double Widget::criteria(const QVector<int> vector, int digitCount)
{
    Q_UNUSED(digitCount)
    return diffCriteria(vector);
}

/*!
 * \brief Widget::iuCriteria
 * \param vector
 * \return показатель случайности
 */
double Widget::iuCriteria(const QVector<int> vector)
{
    if (vector.isEmpty())
        return 0;
    auto minmax = std::minmax_element(vector.begin(), vector.end());
    // Разность между максимальным и минимальным и умножаем на размер выборки
    double divider = (*(minmax.second) - *(minmax.first)) * vector.size();
    if (qFuzzyIsNull(divider)) {
        return 0;
    }

    int sum = 0;
    // Считаем сумму модулей разностей между соседними элементами выборки
    for (int i = 1; i < vector.size(); i++) {
        sum += qAbs(vector.at(i - 1) - vector.at(i));
    }
    return double(sum) / divider;
}

/*!
 * \brief Widget::diffCriteria Разность рядом стоящих элементов - чем ближе
 * к единице, тем лучше.
 * \param vector
 * \return показатель случайности
 *
 * Считается разность между соседними элементами и делится на предыдущую
 * разность. Чем ближе полученное отношение к нулю, тем более случайными
 * являются числа.
 */
double Widget::diffCriteria(const QVector<int> vector)
{
    if (vector.isEmpty())
        return 0;
    if (vector.size() < 3)
        return 0;

    double sum = 0;
    int diffPrev = qAbs(vector.at(0) - vector.at(1));
    for (int i = 2; i < vector.size(); i++) {
        int diff = qAbs(vector.at(i-1) - vector.at(i));
        if (diff != diffPrev) {
            sum += diff > diffPrev ? (double)diffPrev / (double)diff :
                                     (double)diff / (double)diffPrev;
        }
        else
            sum += 1;
        diffPrev = diff;
    }
    return 1 - sum / vector.size();
}

void Widget::on_pushButtonTable_clicked()
{
    QVector<int> one, two, three;
    try {
        three = loadFile(":/tables//123.txt");
        two = loadFile(":/tables//12.txt");
        one = loadFile(":/tables//1.txt");

        int count = qMin(one.count(), qMin(two.count(), three.count()));
        ui->tableWidgetTable->clearContents();
        ui->tableWidgetTable->setRowCount(count);
        for (int i = 0; i < count; i++) {
            ui->tableWidgetTable->setItem(i, 0, new QTableWidgetItem(QString::number(one.at(i))));
            ui->tableWidgetTable->setItem(i, 1, new QTableWidgetItem(QString::number(two.at(i))));
            ui->tableWidgetTable->setItem(i, 2, new QTableWidgetItem(QString::number(three.at(i))));
        }
    }
    catch (QString &e) {
        QMessageBox::critical(this, "Exception", e);
    }

    ui->labelCriteriaTable->setText(QString("%1 || %2 || %3")
                                   .arg(criteria(one, 1), 0, 'g', 4)
                                   .arg(criteria(two, 2), 0, 'g', 4)
                                   .arg(criteria(three, 3), 0, 'g', 4));
}

void Widget::on_pushButtonAlgoGenerate_clicked()
{
    Random randOne(QTime::currentTime().msec()),
            randTwo(QTime::currentTime().msec() / 2),
            randThree(QTime::currentTime().msec() / 2);
    QVector<int> one, two, three;
    const int numberCount = 1000;
    ui->tableWidgetAlgo->clearContents();
    ui->tableWidgetAlgo->setRowCount(numberCount);
    for (int i = 0; i < numberCount; i++) {
        one.append(randOne.next() % 10);
        ui->tableWidgetAlgo->setItem(i, 0, new QTableWidgetItem(QString::number(one.at(i))));
        two.append(randTwo.next() % 100);
        ui->tableWidgetAlgo->setItem(i, 1, new QTableWidgetItem(QString::number(two.at(i))));
        three.append(randThree.next() % 1000);
        ui->tableWidgetAlgo->setItem(i, 2, new QTableWidgetItem(QString::number(three.at(i))));
    }

    ui->labelCriteriaAlgo->setText(QString("%1 || %2 || %3")
                                   .arg(criteria(one, 1), 0, 'g', 4)
                                   .arg(criteria(two, 2), 0, 'g', 4)
                                   .arg(criteria(three, 3), 0, 'g', 4));
}

void Widget::on_pushButtonUserGenerate_clicked()
{
    QVector<int> one, two, three;
    for (int i = 0; i < ui->tableWidgetUser->rowCount(); i++) {
        if (ui->tableWidgetUser->item(i, 0) != 0) {
            bool ok = true;
            int num = ui->tableWidgetUser->item(i, 0)->text().toInt(&ok);
            if (ok) {
                one.append(num);
            }
        }
        if (ui->tableWidgetUser->item(i, 1) != 0) {
            bool ok = true;
            int num = ui->tableWidgetUser->item(i, 1)->text().toInt(&ok);
            if (ok) {
                two.append(num);
            }
        }
        if (ui->tableWidgetUser->item(i, 2) != 0) {
            bool ok = true;
            int num = ui->tableWidgetUser->item(i, 2)->text().toInt(&ok);
            if (ok) {
                three.append(num);
            }
        }
    }
    ui->labelCriteriaUser->setText(QString("%1 || %2 || %3")
                                   .arg(criteria(one, 1), 0, 'g', 4)
                                   .arg(criteria(two, 2), 0, 'g', 4)
                                   .arg(criteria(three, 3), 0, 'g', 4));
}
