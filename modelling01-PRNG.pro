#-------------------------------------------------
#
# Project created by QtCreator 2014-10-15T21:33:13
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = modelling01-PRNG
TEMPLATE = app

CONFIG += c++11

SOURCES += main.cpp\
        widget.cpp \
    random.cpp

HEADERS  += widget.hpp \
    random.hpp

FORMS    += widget.ui

OTHER_FILES += \
    README.md

RESOURCES += \
    resources.qrc
