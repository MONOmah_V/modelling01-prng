﻿#ifndef WIDGET_HPP
#define WIDGET_HPP

#include <QWidget>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private:
    Ui::Widget *ui;

    void setTableWidgetHeaders();

    QVector<int> loadFile(const QString path);
    void fillUserInputTable();

    double chiSquared(const QVector<int> vector, int digitCount);
    double criteria(const QVector<int> vector, int digitCount);

    double iuCriteria(const QVector<int> vector);
    double diffCriteria(const QVector<int> vector);

protected slots:
    void on_pushButtonTable_clicked();
    void on_pushButtonAlgoGenerate_clicked();
    void on_pushButtonUserGenerate_clicked();
};

#endif // WIDGET_HPP
