﻿#include "random.hpp"

Random::Random(uint seed) : xn(seed), a(1103515245), m(0x80000000), c(12345) {}

Random::~Random() {}

int Random::next()
{
    xn = (a * xn + c) % m;
    return xn;
}
