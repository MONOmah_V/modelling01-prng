﻿#ifndef RANDOM_HPP
#define RANDOM_HPP
#include <QString>
#include <QVector>
#include <QTextStream>

// Линейный конгруэнтный генератор
class Random
{
public:
    Random(uint seed);
    virtual ~Random();
    int next();

private:
    // Xn -- значение на предыдущем шаге
    uint xn;
    // a -- множитель
    const int a;
    // m -- модуль, 2^31 = 0x8000 0000
    const uint m;
    // c -- приращение, при c != 0 метод называет смешанным
    const int c;
};

#endif // RANDOM_HPP
